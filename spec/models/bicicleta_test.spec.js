var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');


describe('Testing Bicicletas', function () {

    beforeAll(function (done) {

        mongoose.connection.close().then(() => {
            var mongoDB = 'mongodb://localhost/testdb';
            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
            
            mongoose.set('useCreateIndex', true);
            var db = mongoose.connection;

            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', function () {

                console.log('We are connected to test database!');
                done();

            });
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        })
    })

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', (done) => {
            var bici = Bicicleta.createInstance(1, 'verde', 'urbana', [-34.5, -54.1])

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
            done()
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', function (done) {
            Bicicleta.allBicis(function (err, bicis) {

                expect(bicis.length).toBe(0);

                done();
            })
        })
    })

    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            var aBici = new Bicicleta({ code: 1, color: 'verde', modelo: 'urbana' });
            Bicicleta.add(aBici, function (err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function (err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                });
            });
        });
    })

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({ code: 1, color: 'verde', modelo: 'urbana' });
                Bicicleta.add(aBici, function (err, newBici) {
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({ code: 2, color: 'roja', modelo: 'montaña' });
                    Bicicleta.add(aBici2, function (err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (error, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo)

                            done()
                        })
                    })
                })
            })
        })
    })

    describe('Bicicleta.removeByCode', () => {
        it('se debe eliminar una bici y quedar en 0', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({ code: 1, color: 'verde', modelo: 'urbana' });
                Bicicleta.add(aBici, function (err, newBici) {
                    if (err) console.log(err);

                    Bicicleta.removeByCode(1, function (error, targetBici) {
                        expect(bicis.length).toBe(0);

                        done()
                    })
                })
            })
        })
    })
})


/*
beforeEach(() => {
    Bicicleta.allBicis = []
});

describe ('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe ('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var z = new Bicicleta(1, 'azul', 'montaña', [-34.61, -58,39])
        Bicicleta.add(z);

        //le agrego una para ver si funciona el add
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(z);
    })
})

describe ('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var bici = new Bicicleta(1, 'verde', 'urbana');
        var bici2 = new Bicicleta(2, 'azul', 'urbana');

        Bicicleta.add(bici);
        Bicicleta.add(bici2);

        var targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(bici.color);
        expect(targetBici.modelo).toBe(bici.modelo);
    })
})

describe ('Bicicleta.removeId', () => {
    it('debe eliminar la bici con id 1', () => {
        var bici = new Bicicleta (1, 'verde', 'montaña');
        Bicicleta.add(bici);

        Bicicleta.removeId(bici.id);

        expect(Bicicleta.allBicis.length).toBe(0);
    })
})
*/